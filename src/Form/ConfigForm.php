<?php

namespace Drupal\ridb\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures module settings.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * MyModuleService constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): ConfigForm|ConfigFormBase|static {
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'ridb_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'ridb.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('blm_visitus_ridb.settings');

    $form['ridb_apiurl'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API URL'),
      '#default_value' => $config->get('ridb_apiurl'),
      '#required'      => TRUE,
      '#description'   => $this->t("Enter your API URL for RIDB."),
    ];

    $form['ridb_api'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('API Key'),
      '#default_value' => $config->get('ridb_api'),
      '#required'      => FALSE,
      '#description'   => $this->t("Enter your API Key from RIDB.
      Best practice is to include this key in a secrets file, but this field
      can be used for testing."),
    ];

    $form['ridb_orgid'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('RIDB Organization ID'),
      '#default_value' => $config->get('ridb_orgid'),
      '#required'      => TRUE,
      '#description'   => $this->t("Enter your RIDB Organization ID."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);
    $config = $this->config('ridb.settings');
    $config->set('ridb_api', $form_state->getValue('ridb_api'));
    $config->set('ridb_apiurl', $form_state->getValue('ridb_apiurl'));
    $config->set('ridb_orgid', $form_state->getValue('ridb_orgid'));
    $config->save();
    $this->messenger()->addStatus('Configuration saved.');
  }

}
