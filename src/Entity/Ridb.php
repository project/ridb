<?php

namespace Drupal\ridb\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the RIDB entity.
 *
 * @ingroup ridb
 *
 * @ContentEntityType(
 *   id = "ridb",
 *   label = @Translation("RIDB"),
 *   base_table = "ridb",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "bundle" = "type",
 *   },
 *   handlers = {
 *      "form" = {
 *        "default" = "Drupal\Core\Entity\ContentEntityForm",
 *        "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *      },
 *      "route_provider" = {
 *        "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *      },
 *   },
 *   links = {
 *      "canonical" = "/ridb/{ridb}",
 *      "delete-form" = "/ridb/{ridb}/delete",
 *    },
 *    admin_permission = "administer site configuration",
 *    bundle_entity_type = "ridb_type",
 *    field_ui_base_route = "entity.ridb_type.default",
 * )
 */

class Ridb extends ContentEntityBase implements ContentEntityInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    return parent::baseFieldDefinitions($entity_type);
  }
}