<?php

namespace Drupal\ridb\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * RIDB Type
 *
 * @ConfigEntityType(
 *   id = "ridb_type",
 *   label = @Translation("RIDB Type"),
 *   bundle_of = "ridb",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_prefix = "ridb_type",
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 *   handlers = {
 *     "form" = {
 *       "default" = "Drupal\ridb\Form\RidbTypeEntityForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\ridb\RidbTypeListBuilder",
 *   },
 *   admin_permission = "administer site configuration",
 *   links = {
 *     "canonical" = "/admin/structure/ridb_type/{ridb_type}",
 *     "add-form" = "/admin/structure/ridb_type/add",
 *     "edit-form" = "/admin/structure/ridb_type/{ridb_type}/edit",
 *     "delete-form" = "/admin/structure/ridb_type/{ridb_type}/delete",
 *     "collection" = "/admin/structure/ridb-types",
 *   }
 * )
 */
class RidbType extends ConfigEntityBundleBase {

  /**
   * The machine name of this RIDB type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the RIDB type.
   *
   * @var string
   */
  protected $lable;

}